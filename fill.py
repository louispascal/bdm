import sqlite3
from sqlite3 import Error
import pandas


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def insert_values(conn):
    cur = conn.cursor()
    cur.execute("INSERT INTO Dates VALUES (1,1,1,1970);")
    rows = cur.fetchall()



def main():
    database = r"py/Sqlite.db"
    
    # create a database connection
    conn = create_connection(database)
    with conn:
        insert_values(conn)
        print("Creating Tables")
        cur =  conn.cursor()
        cur.execute("SELECT * FROM Dates;")



if __name__ == '__main__':
    main()