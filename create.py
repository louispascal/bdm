import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def create_table(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("CREATE TABLE Dates(id_date COUNTER,jour INT,mois INT,année INT,PRIMARY KEY(id_date));")
    cur.execute("CREATE TABLE candidat(id_candidat COUNTER,état_civil VARCHAR(50),parti VARCHAR(256),tendance_politique VARCHAR(256),PRIMARY KEY(id_candidat));")
    cur.execute("CREATE TABLE Scrutin(id_scrutin COUNTER,type_de_scrutin VARCHAR(50),tour BYTE,PRIMARY KEY(id_scrutin));")
    cur.execute("CREATE TABLE Géographie(id_géographie COUNTER,circonscription VARCHAR(50),région VARCHAR(50),pays VARCHAR(50),PRIMARY KEY(id_géographie));")
    cur.execute("CREATE TABLE Fait_Resultat(vote INT,__vote VARCHAR(50),id_date INT,id_candidat INT,id_scrutin INT,id_géographie INT,FOREIGN KEY(id_date) REFERENCES Dates(id_date),FOREIGN KEY(id_candidat) REFERENCES candidat(id_candidat),FOREIGN KEY(id_scrutin) REFERENCES Scrutin(id_scrutin),FOREIGN KEY(id_géographie) REFERENCES Géographie(id_géographie)PRIMARY KEY(id_date,id_candidat,id_scrutin,id_géographie));")
    cur.execute("CREATE TABLE Fait_Participation(inscrits INT, B_N INT, exprimés INT, abstention INT, B_N8 INT, Exprimés8 INT, Abstentions8 INT,id_date INT,id_candidat INT,id_scrutin INT,id_géographie INT, FOREIGN KEY(id_date) REFERENCES Dates(id_date),FOREIGN KEY(id_scrutin) REFERENCES Scrutin(id_scrutin),FOREIGN KEY(id_géographie) REFERENCES Géographie(id_géographie) PRIMARY KEY(id_date,id_scrutin,id_géographie));")
    rows = cur.fetchall()



def main():
    database = r"Sqlite.db"
    
    # create a database connection
    conn = create_connection(database)
    with conn:
        create_table(conn)
        print("Creating Tables")



if __name__ == '__main__':
    main()